package com.selena.app.di

import com.selena.app.presentation.launch.HolidaysDataViewAdapter
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { HolidaysDataViewAdapter(get(), get()) }

}
