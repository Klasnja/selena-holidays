package com.selena.app.di

import com.selena.app.data.managers.*
import com.selena.app.di.NetworkModule.Companion.PRE_INIT_SERVICE_API
import com.selena.app.domain.interactors.HolidaysInteractor
import com.selena.app.domain.repositories.*
import com.selena.app.domain.utills.Dispatchers
import com.selena.app.util.EventBus
import com.selena.app.util.Logger
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.bind
import org.koin.dsl.module

@UseExperimental(ExperimentalCoroutinesApi::class)
val appModule = module {

    // region single
    single { AuthManager(get(named(PRE_INIT_SERVICE_API))) } bind AuthRepository::class

    single { HolidaysDataManager(androidContext(), get(), get()) } bind HolidaysDataRepository::class

    single { PublicHolidaysDataManager() } bind PublicHolidaysDataRepository::class

    single { DateCalculationManager(get()) } bind DateCalculationRepository::class

    single { LogManager()} bind LogRepository::class

    single { PersistentManager(androidContext(), "user.data")} bind PersistentRepository::class

    single { Logger()}

    single { EventBus()}

    single { Dispatchers()}
    // end region

    single { HolidaysInteractor(get(), get(), get(), get(), get()) }
}