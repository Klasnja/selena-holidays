package com.selena.app.di

import android.content.Context
import com.selena.app.presentation.components.ErrorComponent
import com.selena.app.presentation.launch.LaunchActivity
import org.koin.core.qualifier.named
import org.koin.dsl.module

val activityModule = module {

    scope(named<LaunchActivity>()) {
        scoped { (activityContext: Context) -> ErrorComponent(activityContext) }
    }
}
