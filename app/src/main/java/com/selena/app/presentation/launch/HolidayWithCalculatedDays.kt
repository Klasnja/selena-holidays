package com.selena.app.presentation.launch

import com.selena.app.data.api.models.ApiModel
import com.selena.app.data.api.models.Holiday

data class HolidayWithCalculatedDays(val holiday: Holiday, var days: ApiModel<Int>? = null)