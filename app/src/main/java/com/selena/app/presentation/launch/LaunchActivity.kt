package com.selena.app.presentation.launch

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.selena.app.R
import com.selena.app.data.api.models.ApiModel
import com.selena.app.data.api.models.Holiday
import com.selena.app.data.api.models.HolidaysData
import com.selena.app.presentation.common.BaseActivity
import com.selena.app.presentation.components.ErrorComponent
import kotlinx.android.synthetic.main.activity_launch.*
import kotlinx.android.synthetic.main.activity_launch_content.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.androidx.scope.currentScope
import org.koin.core.parameter.parametersOf


class LaunchActivity : BaseActivity() {

    private lateinit var adapter: HolidaysDataAdapter

    private val errorComponent: ErrorComponent by currentScope.inject { parametersOf(this) }

    private val holidaysDataViewAdapter: HolidaysDataViewAdapter by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)

        adapter = HolidaysDataAdapter()

        setupRecyclerView()

        swiperefresh.setOnRefreshListener {
            holidaysDataViewAdapter.getFreshData()
        }

        observeViewModel(holidaysDataViewAdapter)
    }

    private fun configureToolbar(title: String) {

        toolbar?.apply {
            setTitle(title)
            setSupportActionBar(this)
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
    }


    private fun observeViewModel(holidaysDataViewAdapter: HolidaysDataViewAdapter) {
        holidaysDataViewAdapter.getLiveHolidaysDataObservable().observe(this, Observer<ApiModel<HolidaysData>> { data ->
            swiperefresh.isRefreshing = false

            data?.apply {
                model?.let { holidaysData ->
                    updateData(holidaysData)
                }
                throwable?.let {
                    errorComponent.handleError(it) {
                        holidaysDataViewAdapter.getFreshData()
                    }
                }
            }
        })

        holidaysDataViewAdapter.getLiveCalculatedDataObservable().observe(this, Observer<Map<Holiday, ApiModel<Int>>> { data ->
            data.entries.forEach { entry ->
                val holiday = entry.key
                val calculatedValue = entry.value
                updateHoliday(holiday, calculatedValue)
            }
        })
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        rv_data.layoutManager = layoutManager
        rv_data.itemAnimator = DefaultItemAnimator()
        rv_data.isNestedScrollingEnabled = true
        rv_data.setHasFixedSize(false)
        rv_data.adapter = adapter
    }

    private fun updateData(data: HolidaysData) {
        val viewItemList = mutableListOf<HolidaysDataAdapter.ViewItem>()


        data.holidays.groupBy { it.destination }.forEach { entry ->

            val destination = entry.key
            val holidays = entry.value.sortedBy { it.startDate }

            viewItemList.add(HolidaysDataAdapter.ViewItem(HolidaysDataAdapter.ViewType.TITLE, destination))
            viewItemList.add(HolidaysDataAdapter.ViewItem(HolidaysDataAdapter.ViewType.DIVIDER, ""))
            holidays.forEach {
                viewItemList.add(HolidaysDataAdapter.ViewItem(HolidaysDataAdapter.ViewType.ITEM, HolidayWithCalculatedDays(it)))
            }
        }

        adapter.updateData(viewItemList)
    }


    private fun updateHoliday(holiday: Holiday, calculatedValue: ApiModel<Int>) {
        adapter.updateItemData(holiday, calculatedValue)
    }
}

