package com.selena.app.presentation.launch

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.selena.app.data.api.models.ApiModel
import com.selena.app.data.api.models.Holiday
import com.selena.app.data.api.models.HolidaysData
import com.selena.app.domain.interactors.HolidaysInteractor
import com.selena.app.presentation.common.BaseViewModel
import kotlinx.coroutines.launch

class HolidaysDataViewAdapter(application: Application,
                              val holidaysInteractor: HolidaysInteractor) : BaseViewModel(application) {

    private val data = MutableLiveData<ApiModel<HolidaysData>>()

    private val calculatedData = MutableLiveData<Map<Holiday, ApiModel<Int>>>()

    init {
        refresh()
    }

    fun getFreshData() {
        refresh()
    }

    private fun refresh() {
        tasksWhileInMemory.add(viewModelScope.launch {
            holidaysInteractor.populateHolidaysData(data, calculatedData)
        })
    }

    fun getLiveHolidaysDataObservable() = data

    fun getLiveCalculatedDataObservable() = calculatedData
}
