package com.selena.app.presentation.components

import android.content.Context
import android.content.Intent
import android.provider.Settings
import com.selena.app.R
import com.selena.app.domain.exceptions.ApplicationException
import com.selena.app.domain.exceptions.NoNetworkException
import com.selena.app.util.getErrorOfType
import com.selena.app.util.showMultiActionDialog
import com.selena.app.util.showSingleActionDialog
import java.net.HttpURLConnection

class ErrorComponent constructor(private val context: Context) {

    fun handleError(throwable: Throwable, retryMethodToInvoke: (() -> Any)? = null) {

        val (retry, title, body) = getErrorTxt(throwable)
        val methodToInvoke = if (retry) retryMethodToInvoke else null
        showError(title, body, methodToInvoke)
    }

    fun getErrorTxt(throwable: Throwable) =
            getErrorOfType(throwable, NoNetworkException::class.java)?.let {
                Triple(true,
                        R.string.error_title_network_connection,
                        context.resources.getString(R.string.error_network_connection))
            } ?: getErrorOfType(throwable, ApplicationException::class.java)?.let {
                val appException = throwable as ApplicationException
                val errorMessage = when {
                    appException.messageResId != null -> context.resources.getString(appException.messageResId)
                    appException.bodyMessage != null && permittedResponseCodes.contains(appException.responseCode) -> appException.bodyMessage
                    else -> context.resources.getString(R.string.error_server)
                }
                Triple(false,
                        appException.title,
                        errorMessage)
            } ?: run {
                Triple(false,
                        R.string.error_title_server,
                        context.resources.getString(R.string.error_server))
            }

    private val permittedResponseCodes = listOf(
            HttpURLConnection.HTTP_BAD_REQUEST,
            HttpURLConnection.HTTP_UNAUTHORIZED,
            HttpURLConnection.HTTP_FORBIDDEN,
            HttpURLConnection.HTTP_NOT_FOUND,
            HttpURLConnection.HTTP_UNAVAILABLE)


    private fun showError(title: Int?, body: String, methodToInvoke: (() -> Any)?) {
        methodToInvoke?.let {
            showSingleActionDialog(context,
                    title,
                    body,
                    R.string.retry,
                    { methodToInvoke.invoke() },
                    true)
        } ?: run {
            showSingleActionDialog(context,
                    title,
                    body,
                    R.string.ok_got_it,
                    {},
                    true)
        }
    }
}