package com.selena.app.presentation.launch

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.selena.app.R
import com.selena.app.data.api.models.ApiModel
import com.selena.app.data.api.models.Holiday
import com.selena.app.util.formatDateDDMMMYYYY
import kotlinx.android.synthetic.main.layout_account_item.view.*
import kotlinx.android.synthetic.main.layout_title_item.view.*

class HolidaysDataAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val viewItemList = mutableListOf<ViewItem>()

    data class ViewItem(val viewType: ViewType, val data: Any)

    enum class ViewType {
        TITLE,
        ITEM,
        DIVIDER,
        ERROR
    }

    fun updateData(itemList: List<ViewItem>) {
        viewItemList.clear()
        viewItemList.addAll(itemList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = when (viewType) {
        ViewType.TITLE.ordinal -> {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_title_item, parent, false)
            TitleItemViewHolder(view)
        }
        ViewType.ITEM.ordinal -> {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_account_item, parent, false)
            ItemViewHolder(view)
        }
        ViewType.DIVIDER.ordinal -> {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_divider_item, parent, false)
            DividerItemViewHolder(view)
        }

        else -> throw RuntimeException("not view type matched")
    }


    override fun getItemViewType(position: Int): Int = viewItemList[position].viewType.ordinal

    override fun getItemCount() = viewItemList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is TitleItemViewHolder -> {
                holder.bindData(viewItemList[position].data as String)
            }
            is ItemViewHolder -> {
                holder.bindData(viewItemList[position].data as HolidayWithCalculatedDays)
            }
            is DividerItemViewHolder -> {
                holder.bindData()
            }
        }
    }

    fun updateItemData(holiday: Holiday, calculatedValue: ApiModel<Int>) {
        val holidayIndex = viewItemList.indexOfFirst { it.viewType == ViewType.ITEM && (it.data as HolidayWithCalculatedDays).holiday.id == holiday.id }
        if (holidayIndex != -1) {
            val holidayWithCalculatedDays = viewItemList[holidayIndex].data as HolidayWithCalculatedDays
            if(holidayWithCalculatedDays.days != calculatedValue) {
                holidayWithCalculatedDays.days = calculatedValue
                notifyItemChanged(holidayIndex)
            }
        }
    }

    class TitleItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(title: String) {
            with(itemView) {
                txt_title.text = title
            }
        }
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(holidayWithCalculatedDays: HolidayWithCalculatedDays) {
            with(itemView) {
                val holiday = holidayWithCalculatedDays.holiday
                val text = "${holiday.startDate.formatDateDDMMMYYYY()} - ${holiday.endDate.formatDateDDMMMYYYY()}"
                txt_name.text = text

                val weekDays = holidayWithCalculatedDays.days
                val daysText = when {
                    weekDays == null -> "Calculating"
                    weekDays.throwable != null -> "Error"
                    else -> weekDays.model.toString() + "d"
                }
                txt_value.text = daysText

            }
        }
    }

    class DividerItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData() {
        }
    }
}