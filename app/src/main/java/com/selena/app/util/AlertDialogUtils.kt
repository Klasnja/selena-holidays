package com.selena.app.util

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.WindowManager
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import com.selena.app.presentation.common.BaseActivity

fun Activity.showSingleActionDialog(@StringRes titleResId: Int,
                                    @StringRes messageResId: Int,
                                    @StringRes positiveButtonTextResId: Int,
                                    positiveAction: () -> Unit,
                                    cancellable: Boolean = true): AlertDialog {
    return showActionDialog(this, getString(titleResId), getString(messageResId), positiveButtonTextResId, positiveAction,
            null, {}, null, {}, cancellable)
}

fun Activity.showSingleActionDialog(title: String,
                                    message: String?,
                                    @StringRes positiveButtonTextResId: Int,
                                    positiveAction: () -> Unit,
                                    cancellable: Boolean = true): AlertDialog {
    return showActionDialog(this, title, message, positiveButtonTextResId, positiveAction,
            null, {}, null, {}, cancellable)
}

fun showSingleActionDialog(context: Context,
                           @StringRes titleResId: Int,
                           @StringRes messageResId: Int,
                           @StringRes positiveButtonTextResId: Int,
                           positiveAction: () -> Unit,
                           cancellable: Boolean = true): AlertDialog {
    return showActionDialog(context, context.getString(titleResId), context.getString(messageResId), positiveButtonTextResId, positiveAction,
            null, {}, null, {}, cancellable)
}

fun showSingleActionDialog(context: Context,
                           @StringRes titleResId: Int?,
                           message: String?,
                           @StringRes positiveButtonTextResId: Int,
                           positiveAction: () -> Unit,
                           cancellable: Boolean = true): AlertDialog {
    val title = titleResId?.run { context.getString(titleResId) }
    return showActionDialog(context, title, message, positiveButtonTextResId, positiveAction,
            null, {}, null, {}, cancellable)
}

fun Activity.showMultiActionDialog(@StringRes titleResId: Int,
                                   @StringRes positiveButtonTextResId: Int,
                                   positiveAction: () -> Unit,
                                   @StringRes negativeButtonTextResId: Int,
                                   negativeAction: () -> Unit,
                                   cancellable: Boolean = true): AlertDialog {
    return showActionDialog(this, getString(titleResId), null as String?, positiveButtonTextResId,
            positiveAction, negativeButtonTextResId, negativeAction, null, {}, cancellable)
}

fun Activity.showMultiActionDialog(@StringRes titleResId: Int,
                                   @StringRes messageResId: Int,
                                   @StringRes positiveButtonTextResId: Int,
                                   positiveAction: () -> Unit,
                                   @StringRes negativeButtonTextResId: Int,
                                   negativeAction: () -> Unit,
                                   cancellable: Boolean = true): AlertDialog {
    return showActionDialog(this, getString(titleResId), getString(messageResId), positiveButtonTextResId,
            positiveAction, negativeButtonTextResId, negativeAction, null, {}, cancellable)
}

fun Activity.showMultiActionDialog(@StringRes titleResId: Int,
                                   @StringRes messageResId: Int,
                                   @StringRes positiveButtonTextResId: Int,
                                   positiveAction: () -> Unit,
                                   @StringRes negativeButtonTextResId: Int,
                                   negativeAction: () -> Unit,
                                   @StringRes neutralButtonTextResId: Int,
                                   neutralAction: () -> Unit,
                                   cancellable: Boolean = true): AlertDialog {
    return showActionDialog(this, getString(titleResId), getString(messageResId), positiveButtonTextResId,
            positiveAction, negativeButtonTextResId, negativeAction, neutralButtonTextResId, neutralAction, cancellable)
}

fun showMultiActionDialog(context: Context,
                          @StringRes titleResId: Int,
                          @StringRes messageResId: Int,
                          @StringRes positiveButtonTextResId: Int,
                          positiveAction: () -> Unit,
                          @StringRes negativeButtonTextResId: Int,
                          negativeAction: () -> Unit,
                          cancellable: Boolean = true): AlertDialog {
    return showActionDialog(context, context.getString(titleResId), context.getString(messageResId), positiveButtonTextResId,
            positiveAction, negativeButtonTextResId, negativeAction, null, {}, cancellable)
}

fun showMultiActionDialog(context: Context,
                          @StringRes titleResId: Int?,
                          message: String?,
                          @StringRes positiveButtonTextResId: Int,
                          positiveAction: () -> Unit,
                          @StringRes negativeButtonTextResId: Int,
                          negativeAction: () -> Unit,
                          cancellable: Boolean = true): AlertDialog {
    val title = titleResId?.run { context.getString(titleResId) }
    return showActionDialog(context, title, message, positiveButtonTextResId,
            positiveAction, negativeButtonTextResId, negativeAction, null, {}, cancellable)
}

private fun showActionDialog(context: Context,
                             title: String?,
                             message: String?,
                             @StringRes positiveButtonTextResId: Int,
                             positiveAction: () -> Unit,
                             @StringRes negativeButtonTextResId: Int?,
                             negativeAction: () -> Unit,
                             @StringRes neutralButtonTextResId: Int?,
                             neutralAction: () -> Unit,
                             cancellable: Boolean = true): AlertDialog {
    val alertDialogBuilder = AlertDialog.Builder(context)
            .setCancelable(cancellable)
            .setTitle(title)
            .setPositiveButton(context.getString(positiveButtonTextResId)) { _, _ -> positiveAction() }

    message?.let {
        alertDialogBuilder.setMessage(it)
    }

    negativeButtonTextResId?.let {
        alertDialogBuilder.setNegativeButton(context.getString(it)) { _, _ -> negativeAction() }
    }

    neutralButtonTextResId?.let {
        alertDialogBuilder.setNeutralButton(context.getString(it)) { _, _ -> neutralAction() }
    }

    val alertDialog = alertDialogBuilder.create()
    alertDialog.show()

    if (context is BaseActivity) {
        context.setCurrentPopupDialog(alertDialog)
    }
    return alertDialog
}

fun alertDialogWithMultiChoice(context: Context,
                               title: String,
                               @StringRes positiveButtonTextResId: Int,
                               positiveAction: () -> Unit,
                               @StringRes negativeButtonTextResId: Int?,
                               negativeAction: () -> Unit,
                               cancellable: Boolean = true,
                               optionsTitlesArray: Array<String?>?,
                               optionsCheckedArray: BooleanArray,
                               multiItemClick: (Int, Boolean) -> Unit) {

    val alertDialogBuilder = AlertDialog.Builder(context)
            .setCancelable(cancellable)
            .setTitle(title)
            .setMultiChoiceItems(optionsTitlesArray, optionsCheckedArray) { _, which, isChecked -> multiItemClick(which, isChecked) }
            .setPositiveButton(context.getString(positiveButtonTextResId)) { _, _ -> positiveAction() }

    negativeButtonTextResId?.let {
        alertDialogBuilder.setNegativeButton(context.getString(it)) { _, _ -> negativeAction() }
    }

    val alertDialog = alertDialogBuilder.create()
    alertDialog.show()

    if (context is BaseActivity) {
        context.setCurrentPopupDialog(alertDialog)
    }
}

fun alertDialogWithCustomView(context: Context,
                              title: String,
                              customView: View,
                              @StringRes positiveButtonTextResId: Int,
                              positiveAction: () -> Unit,
                              @StringRes negativeButtonTextResId: Int?,
                              negativeAction: () -> Unit,
                              cancellable: Boolean = true) {

    val alertDialogBuilder = AlertDialog.Builder(context)
            .setCancelable(cancellable)
            .setTitle(title)
            .setView(customView)
            .setPositiveButton(context.getString(positiveButtonTextResId)) { _, _ -> positiveAction() }

    negativeButtonTextResId?.let {
        alertDialogBuilder.setNegativeButton(context.getString(it)) { _, _ -> negativeAction() }
    }

    val alertDialog = alertDialogBuilder.create()
    alertDialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
    alertDialog.show()

    if (context is BaseActivity) {
        context.setCurrentPopupDialog(alertDialog)
    }
}
