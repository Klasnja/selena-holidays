package com.selena.app.util

import java.text.SimpleDateFormat
import java.util.*


fun Date.formatDateDDMMMYYYY(timeZone: String? = null): String {
    val dateFormat = SimpleDateFormat("d MMM yyyy", Locale.US)
    dateFormat.timeZone = getTimeZone(timeZone)
    return dateFormat.format(this.time)
}

fun String.fromDDMMMYYYtoDate(timeZone: String? = null): Date {
    val dateFormat = SimpleDateFormat("d MMM yyyy", Locale.US)
    dateFormat.timeZone = getTimeZone(timeZone)
    return dateFormat.parse(this)
}

fun getTimeZone(timeZone: String?) = timeZone?.let { TimeZone.getTimeZone(it) }
        ?: TimeZone.getDefault()

fun Date.isWeekDay() : Boolean{
    val c = Calendar.getInstance()
    c.time = this
    val dayOfWeek = c.get(Calendar.DAY_OF_WEEK)
    return dayOfWeek != Calendar.SATURDAY && dayOfWeek != Calendar.SUNDAY
}


fun Calendar.setStartOfDay()  {
    set(Calendar.HOUR_OF_DAY, getActualMinimum(Calendar.HOUR_OF_DAY))
    set(Calendar.MINUTE, getActualMinimum(Calendar.MINUTE))
    set(Calendar.SECOND, getActualMinimum(Calendar.SECOND))
    set(Calendar.MILLISECOND, getActualMinimum(Calendar.MILLISECOND))
}