package com.selena.app.data.managers


import com.selena.app.domain.repositories.DateCalculationRepository
import com.selena.app.domain.repositories.PublicHolidaysDataRepository
import com.selena.app.util.isWeekDay
import com.selena.app.util.setStartOfDay
import java.util.*


class DateCalculationManager(private val publicHolidaysDataRepository: PublicHolidaysDataRepository): DateCalculationRepository {

    override fun getWeekDates(startDate: Date, endDate: Date): Int {

        var totalDays = 0

        val calStartDate = Calendar.getInstance()
        calStartDate.time = startDate
        calStartDate.setStartOfDay()

        // we don't count from date
        calStartDate.add(Calendar.DATE, 1)


        val calEndDate = Calendar.getInstance()
        calEndDate.time = endDate
        calEndDate.setStartOfDay()

        // the sigh is < cause we don't count to date
        while(calStartDate.time.time < calEndDate.time.time){
            if(calStartDate.time.isWeekDay() && !publicHolidaysDataRepository.isHoliday(calStartDate.time)){
                totalDays ++
            }
            calStartDate.add(Calendar.DATE, 1)
        }

        return totalDays
    }
}

