package com.selena.app.data.managers.publicholidays

import com.selena.app.data.managers.IPublicHoliday
import com.selena.app.util.setStartOfDay
import java.util.*

class AnsacDay() : IPublicHoliday {
    override fun isHoliday(date: Date): Boolean {
        val cal = Calendar.getInstance()
        cal.time = date
        cal.setStartOfDay()
        val dayOfMonth = cal.get(Calendar.DAY_OF_MONTH)
        val month = cal.get(Calendar.MONTH)
        return dayOfMonth == 25 && month == 3
    }
}