package com.selena.app.data.api.models

import com.google.gson.annotations.SerializedName
import java.util.*

data class HolidaysData(@SerializedName("holidays") val holidays: List<Holiday>)

data class Holiday(@SerializedName("id") val id: Long,
                   @SerializedName("destination") val destination: String,
                   @SerializedName("start_date") val startDate: Date,
                   @SerializedName("end_date") val endDate: Date)
