package com.selena.app.data.managers

import android.content.Context
import com.google.gson.Gson
import com.selena.app.R
import com.selena.app.data.api.models.HolidaysData
import com.selena.app.domain.repositories.HolidaysDataRepository
import java.io.InputStreamReader


class HolidaysDataManager constructor(private val appContext: Context,
                                      private val persistentManager: PersistentManager,
                                      private val gson: Gson) : HolidaysDataRepository {

    companion object {
        const val HOLIDAYS_DATA = "PUBLIC_HOLIDAYS_DATA"
    }

    override suspend fun getHolidays(): HolidaysData {
        val holidaysDataJson = persistentManager.getData(HOLIDAYS_DATA, null)

        val holidaysData =
                if (holidaysDataJson == null) {
                    val stream = appContext.resources.openRawResource(R.raw.holidays)
                    val reader = InputStreamReader(stream)
                    gson.fromJson(reader, HolidaysData::class.java)
                } else {
                    gson.fromJson(holidaysDataJson, HolidaysData::class.java)
                }
        return holidaysData
    }

}

