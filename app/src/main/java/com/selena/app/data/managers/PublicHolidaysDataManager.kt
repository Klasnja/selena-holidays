package com.selena.app.data.managers

import com.selena.app.data.managers.publicholidays.AnsacDay
import com.selena.app.data.managers.publicholidays.NewYear
import com.selena.app.data.managers.publicholidays.QueenBirthday
import com.selena.app.domain.repositories.PublicHolidaysDataRepository
import com.selena.app.util.setStartOfDay
import java.util.*


class PublicHolidaysDataManager : PublicHolidaysDataRepository {
    companion object {
        val publicHolidays = listOf(AnsacDay(), NewYear(), QueenBirthday())
    }


    override fun isHoliday(date: Date) : Boolean {

        return publicHolidays.firstOrNull{it.isHoliday(date)} != null
    }
}

