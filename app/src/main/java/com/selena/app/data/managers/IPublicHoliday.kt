package com.selena.app.data.managers

import java.util.*

interface IPublicHoliday {
    fun isHoliday(date: Date): Boolean
}