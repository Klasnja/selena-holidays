package com.selena.app.data.api.models

import com.google.gson.annotations.SerializedName

data class PublicHolidaysData(@SerializedName("holidays") val holidays: List<PublicHoliday>)

data class PublicHoliday(@SerializedName("id") val id: Long,
                   @SerializedName("name") val name: String,
                   @SerializedName("date") val date: String)
