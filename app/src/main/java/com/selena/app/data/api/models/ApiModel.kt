package com.selena.app.data.api.models

data class ApiModel<Model>(val model: Model? = null, val throwable: Throwable? = null)