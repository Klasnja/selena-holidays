package com.selena.app.data.managers

import android.content.Context
import android.content.SharedPreferences
import com.selena.app.domain.repositories.PersistentRepository

class PersistentManager (appContext: Context, filename: String) : PersistentRepository {

    private val sharedPreferences: SharedPreferences = appContext.getSharedPreferences(filename, Context.MODE_PRIVATE)

    override fun saveBoolean(key: String, value: Boolean) {
        sharedPreferences.edit().putBoolean(key, value).apply()
    }

    override fun getBoolean(key: String, default: Boolean) = sharedPreferences.getBoolean(key, default)

    override fun saveData(key: String, value: String?) {
        sharedPreferences.edit().putString(key, value).apply()
    }

    override fun getData(key: String, default: String?): String?
            = sharedPreferences.getString(key, default)

    override fun saveStringSet(key: String, value: Set<String>?) {
        sharedPreferences.edit().putStringSet(key, value).apply()
    }

    override fun getStringSet(key: String, default: Set<String>?): Set<String>?
            = sharedPreferences.getStringSet(key, default)
}