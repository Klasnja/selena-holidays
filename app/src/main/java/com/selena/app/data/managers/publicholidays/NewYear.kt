package com.selena.app.data.managers.publicholidays

import com.selena.app.data.managers.IPublicHoliday
import com.selena.app.util.isWeekDay
import com.selena.app.util.setStartOfDay
import java.util.*

class NewYear() : IPublicHoliday {
    override fun isHoliday(date: Date): Boolean {
        val cal = Calendar.getInstance()
        cal.time = date
        cal.setStartOfDay()
        val dayOfWeek = cal.get(Calendar.DAY_OF_WEEK)
        val dayOfMonth = cal.get(Calendar.DAY_OF_MONTH)
        val month = cal.get(Calendar.MONTH)
        val isWeekDay = cal.time.isWeekDay()
        // if it is not january of it is not a week day return false
        if(month != 0 || !isWeekDay) return false


        // it is 1st or it is Monday and January and 2nd of 3rd
        return (dayOfMonth == 1) || (dayOfWeek == 2 &&  (dayOfMonth == 2 || dayOfMonth == 3))
    }
}