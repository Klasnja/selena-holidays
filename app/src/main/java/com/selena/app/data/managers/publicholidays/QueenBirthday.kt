package com.selena.app.data.managers.publicholidays

import com.selena.app.data.managers.IPublicHoliday
import com.selena.app.util.setStartOfDay
import java.util.*

class QueenBirthday() : IPublicHoliday {
    override fun isHoliday(date: Date): Boolean {
        val cal = Calendar.getInstance()
        cal.time = date
        cal.setStartOfDay()

        val dayOfWeek = cal.get(Calendar.DAY_OF_WEEK)
        val weekOfMonth = cal.get(Calendar.WEEK_OF_MONTH)
        val month = cal.get(Calendar.MONTH)

        val dayOfMonth = cal.get(Calendar.DAY_OF_MONTH)

        // if it is not June and Monday return false
        if(month != 5 || dayOfWeek != 2) return false

        return (weekOfMonth == 2 && dayOfMonth == 8) || (weekOfMonth == 3 && dayOfMonth > 8 && dayOfMonth < 15)
    }
}