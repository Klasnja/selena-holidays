package com.selena.app.domain.interactors

import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.selena.app.data.api.models.ApiModel
import com.selena.app.data.api.models.Holiday
import com.selena.app.data.api.models.HolidaysData
import com.selena.app.domain.repositories.DateCalculationRepository
import com.selena.app.domain.repositories.HolidaysDataRepository
import com.selena.app.domain.repositories.LogRepository
import com.selena.app.domain.utills.Dispatchers
import kotlinx.coroutines.*
import java.util.*


class HolidaysInteractor(private val holidaysDataRepository: HolidaysDataRepository,
                         private val dateCalculationRepository: DateCalculationRepository,
                         logRepository: LogRepository,
                         dispatchers: Dispatchers,
                         gson: Gson) : BaseInteractor(logRepository, dispatchers, gson) {


    suspend fun populateHolidaysData(data: MutableLiveData<ApiModel<HolidaysData>>, calculatedDays: MutableLiveData<Map<Holiday, ApiModel<Int>>>) {
        logRepository.e("Entry", "   'getHolidaysTask api call': I'm working in thread ${Thread.currentThread().name}")
        val result = getHolidaysData()
        data.postValue(result)
        getCalculatedWeekdaysInParalel(result, calculatedDays)
    }

    private suspend fun getCalculatedWeekdaysInParalel(result: ApiModel<HolidaysData>, calculatedDays: MutableLiveData<Map<Holiday, ApiModel<Int>>>) {
        val tasks = mutableListOf<Deferred<Any>>()
        result.model?.apply {
            holidays.forEach { holiday ->
                tasks.add(populateWeekDaysCalculation(holiday, calculatedDays))
            }
        }
        tasks.awaitAll()
    }


    suspend fun populateWeekDaysCalculation(holiday: Holiday, calculatedDays: MutableLiveData<Map<Holiday, ApiModel<Int>>>) = coroutineScope {
        async(dispatchers.io()) {
            logRepository.e("Entry", "   'api call': I'm working in thread ${Thread.currentThread().name}")
            val result = getWeekDates(holiday.startDate, holiday.endDate)
            withContext(dispatchers.ui()) {
                val map = calculatedDays.value?.toMutableMap() ?: mutableMapOf()
                map[holiday] = result
                calculatedDays.value = map
            }
        }
    }

    private suspend fun getHolidaysData(): ApiModel<HolidaysData> =
            try {
                ApiModel(holidaysDataRepository.getHolidays())
            } catch (throwable: Throwable) {
                ApiModel(throwable = throwable)
            }


    private fun getWeekDates(startDate: Date, endDate: Date): ApiModel<Int> =
            try {
                ApiModel(dateCalculationRepository.getWeekDates(startDate, endDate))
            } catch (throwable: Throwable) {
                ApiModel(throwable = throwable)
            }

}
