package com.selena.app.domain.repositories

import java.util.*

interface PublicHolidaysDataRepository {

    fun isHoliday(date: Date): Boolean
}