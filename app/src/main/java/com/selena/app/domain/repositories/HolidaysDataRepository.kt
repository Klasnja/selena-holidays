package com.selena.app.domain.repositories

import com.selena.app.data.api.models.HolidaysData

interface HolidaysDataRepository {

    suspend fun getHolidays(): HolidaysData
}