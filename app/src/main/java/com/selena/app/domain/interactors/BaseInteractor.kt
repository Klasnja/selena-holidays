package com.selena.app.domain.interactors

import com.google.gson.Gson
import com.selena.app.domain.repositories.LogRepository
import com.selena.app.domain.utills.Dispatchers

open class BaseInteractor(val logRepository: LogRepository, val dispatchers: Dispatchers, val gson: Gson) {



}