package com.selena.app.domain.repositories


import com.selena.app.data.api.models.responses.UserResponseData
import retrofit2.Call

interface AuthRepository {

    fun loginWithEmail(email: String, password: String): Call<UserResponseData>
}