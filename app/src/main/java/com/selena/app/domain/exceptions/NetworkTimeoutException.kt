package com.selena.app.domain.exceptions

class NetworkTimeoutException : BaseException()