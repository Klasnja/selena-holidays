package com.selena.app.domain.utills

import com.google.gson.Gson
import com.google.gson.stream.MalformedJsonException
import com.selena.app.domain.exceptions.ApplicationException
import com.selena.app.domain.exceptions.NetworkTimeoutException
import com.selena.app.domain.exceptions.NoNetworkException
import com.selena.app.domain.exceptions.ServerApiException
import com.selena.app.domain.helpers.ErrorBodyParser
import okhttp3.Response
import java.net.*

class ExceptionUtil {

    companion object {
        fun mapApiException(gson: Gson, response: Response): ApplicationException {
            return when (response.code()) {
                HttpURLConnection.HTTP_BAD_REQUEST,
                HttpURLConnection.HTTP_UNAUTHORIZED,
                HttpURLConnection.HTTP_FORBIDDEN,
                HttpURLConnection.HTTP_UNAVAILABLE -> {
                    ErrorBodyParser.parse(gson, response)
                }
                else -> ApplicationException(ServerApiException())
            }
        }

        fun mapException(exception: Throwable): Throwable {
            return when (exception) {
                is ConnectException -> NoNetworkException()
                is SocketException -> NoNetworkException()
                is UnknownHostException -> NoNetworkException()
                is SocketTimeoutException -> NetworkTimeoutException()
                is MalformedJsonException -> ApplicationException(exception)
                is ApplicationException -> exception
                else -> ApplicationException(exception)
            }
        }
    }
}