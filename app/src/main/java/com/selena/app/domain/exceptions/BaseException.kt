package com.selena.app.domain.exceptions

open class BaseException : Exception {
    constructor() : super()
    constructor(cause: Throwable) : super(cause)
}