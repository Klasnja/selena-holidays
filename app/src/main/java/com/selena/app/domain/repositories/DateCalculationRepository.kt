package com.selena.app.domain.repositories

import java.util.*

interface DateCalculationRepository {

    fun getWeekDates(startDate: Date, endDate: Date): Int
}