package com.selena.app.util

import com.selena.app.domain.utills.Dispatchers
import kotlinx.coroutines.CoroutineDispatcher

class TestDispatchers : Dispatchers() {
     override fun ui(): CoroutineDispatcher {
        return kotlinx.coroutines.Dispatchers.Unconfined
    }

    override fun io(): CoroutineDispatcher {
        return kotlinx.coroutines.Dispatchers.Unconfined
    }

    override fun default(): CoroutineDispatcher {
        return kotlinx.coroutines.Dispatchers.Unconfined
    }

    override fun unconfirmed(): CoroutineDispatcher {
        return kotlinx.coroutines.Dispatchers.Unconfined
    }
}