package com.selena.app.domain.interactors

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import com.selena.app.data.api.models.ApiModel
import com.selena.app.data.api.models.Holiday
import com.selena.app.data.api.models.HolidaysData
import com.selena.app.domain.repositories.DateCalculationRepository
import com.selena.app.domain.repositories.HolidaysDataRepository
import com.selena.app.domain.repositories.LogRepository
import com.selena.app.util.TestDispatchers
import com.selena.app.util.fromDDMMMYYYtoDate
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.*
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class HolidaysInteractorTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var holidaysDataRepository: HolidaysDataRepository

    @Mock
    lateinit var dateCalculationRepository: DateCalculationRepository

    @Mock
    lateinit var logRepository: LogRepository

    @Mock
    lateinit var gson: Gson

    private val dispatchers = TestDispatchers()

    private lateinit var interactor: HolidaysInteractor

    // region Default values
    private val holidays = listOf(
            Holiday(1, "destination1", "1 May 2019".fromDDMMMYYYtoDate(),
            "11 May 2019".fromDDMMMYYYtoDate()),
            Holiday(2, "destination2", "5 Nov 2019".fromDDMMMYYYtoDate(),
                    "15 Nov 2019".fromDDMMMYYYtoDate()))
    private val holidaysData = HolidaysData(holidays)
    private val numWeekDays = 1
    // end region


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        interactor = HolidaysInteractor(holidaysDataRepository, dateCalculationRepository, logRepository, dispatchers, gson)
    }

    @Test
    fun populateHolidaysData_apiIsSuccessful_resultIsCorrect() = runBlocking {
        // given
        val data = MutableLiveData<ApiModel<HolidaysData>>()
        val calculatedData = MutableLiveData<Map<Holiday, ApiModel<Int>>>()
        data.observeForever {}
        calculatedData.observeForever {  }

        whenever(holidaysDataRepository.getHolidays())
                .thenReturn(holidaysData)

        whenever(dateCalculationRepository.getWeekDates(any(), any()))
                .thenReturn(numWeekDays)

        // when
        interactor.populateHolidaysData(data, calculatedData)

        // then
        assertThat(data.value, `is`(notNullValue()))
        assertThat(data.value!!.throwable, `is`(nullValue()))
        assertThat(data.value!!.model, `is`(notNullValue()))
        assertThat(data.value!!.model!!.holidays, `is`(holidays))

        assertThat(calculatedData.value, `is`(notNullValue()))
        assertThat(calculatedData.value!!.size, `is`(2))
        assertThat(calculatedData.value!![holidays[0]], `is`(ApiModel(numWeekDays)))
    }

    @Test
    fun populateHolidaysData_apiFails_resultIsCorrect() = runBlocking {
        // given
        val data = MutableLiveData<ApiModel<HolidaysData>>()
        val calculatedData = MutableLiveData<Map<Holiday, ApiModel<Int>>>()
        data.observeForever {}

        val exception : Throwable = RuntimeException("An error")
        whenever(holidaysDataRepository.getHolidays())
                .thenThrow(exception)

        // when
        interactor.populateHolidaysData(data, calculatedData)

        // then
        assertThat(data.value, `is`(notNullValue()))
        assertThat(data.value!!.model, `is`(nullValue()))
        assertThat(data.value!!.throwable, `is`(notNullValue()))
        assertThat(data.value!!.throwable, `is`(exception))
    }
}