package com.selena.app.data.managers

import com.selena.app.util.fromDDMMMYYYtoDate
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class PublicHolidaysDataManagerTest{
    private val manager = PublicHolidaysDataManager()


    @Test
    fun isPublicHoliday_isAnsacDay() {

        // when
        val date = "25 Apr 2019".fromDDMMMYYYtoDate()


        val isHoliday = manager.isHoliday(date )

        // then
        assertThat(isHoliday, `is`(true))

    }

    @Test
    fun isPublicHoliday_isNewYearDay() {

        // when
        val date = "2 Jan 2017".fromDDMMMYYYtoDate()


        val isHoliday = manager.isHoliday(date )

        // then
        assertThat(isHoliday, `is`(true))

    }


    @Test
    fun isPublicHoliday_QueensBirthday() {

        // when
        val date = "10 Jun 2019".fromDDMMMYYYtoDate()


        val isHoliday = manager.isHoliday(date )

        // then
        assertThat(isHoliday, `is`(true))

    }
}
