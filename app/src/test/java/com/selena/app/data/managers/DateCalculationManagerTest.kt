package com.selena.app.data.managers

import com.selena.app.util.fromDDMMMYYYtoDate
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.MockitoAnnotations

class DateCalculationManagerTest {


    private var publicHolidaysDataRepository = PublicHolidaysDataManager()

    private lateinit var manager : DateCalculationManager

            @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        manager = DateCalculationManager(publicHolidaysDataRepository)
    }

    @Test
    fun getWeekDates() {

        // when
        val startDate = "6 Jun 2019".fromDDMMMYYYtoDate()
        val endDate = "12 Jun 2019".fromDDMMMYYYtoDate()

        val days = manager.getWeekDates(startDate, endDate )

        // then
        assertThat(days, `is`(2))
    }
}